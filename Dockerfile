FROM openjdk:8

RUN git clone https://mkazm@bitbucket.org/mkazm/home-center.git /home-center-code && \
    cd /home-center-code && \
    chmod +x gradlew && \
    ./gradlew build -x test && \
    cd .. && \
    mkdir /home-center && \
    mv /home-center-code/build/libs/home-center*.jar /home-center/

EXPOSE 8080

WORKDIR /data

VOLUME ["/data"]

CMD exec java -jar /home-center/home-center*.jar --spring.datasource.url=jdbc:mysql://172.17.0.2:3306/metric