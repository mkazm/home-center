package com.kazm.hc

import java.time.LocalDateTime

data class Request (
        var ip: String? = null,
        var uri: String? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now()
)