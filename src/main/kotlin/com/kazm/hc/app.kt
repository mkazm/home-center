package com.kazm.hc

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestParam
import java.math.BigDecimal
import java.math.RoundingMode
import javax.websocket.server.PathParam


@RestController
class HelloController(val temperatureDao: TemperatureDao, val humidityDao: HumidityDao,
                      val temperatureMainRoomDao: TemperatureMainRoomDao, val humidityMainRoomDao: HumidityMainRoomDao,
                      val batteryDao: BatteryDao, val temperatureOutsideDao: TemperatureOutsideDao,
                      val humidityOutsideDao: HumidityOutsideDao, val batteryOutsideDao: BatteryOutsideDao,
                      val batteryBedRoomDao: BatteryBedRoomDao, val batteryDisplayDao: BatteryDisplayDao,
                      val moistureDao: MoistureDao) {

    private val log = logger()

    @Autowired
    val dynamicConfig: DynamicConfigs? = null

    @Autowired
    val myAppConfig: MyAppConfig? = null

    @GetMapping("/")
    fun hello(): String {
        log.info("hi from Home Center")
        return "hi from from Home Center"
    }

    @Transactional
    @GetMapping("/moisture")
    fun moisture(@RequestParam plant: String, @RequestParam moisture: String): String {
        val moisture = Moisture(plant, BigDecimal(moisture))
        moistureDao.save(moisture)
        return "ok"
    }

    //////

    @Transactional
    @GetMapping("/env")
    fun environment(@RequestParam temp: String, @RequestParam hum: String): String {
        val temperature = Temperature(BigDecimal(temp))
        temperatureDao.save(temperature)
        val humidity = Humidity(BigDecimal(hum))
        humidityDao.save(humidity)
        return "ok"
    }

    @Transactional
    @GetMapping("/battery")
    fun battery(@RequestParam unit: String, @RequestParam voltage: String): String {
        val batteryTest = Battery(unit, BigDecimal(voltage))
        batteryDao.save(batteryTest)
        return "ok"
    }

    @Transactional
    @GetMapping("/battery/outside")
    fun batteryOutside(@RequestParam unit: String, @RequestParam voltage: String): String {
        val batteryTest = BatteryOutside(unit, BigDecimal(voltage))
        batteryOutsideDao.save(batteryTest)
        return "ok"
    }

    @Transactional
    @GetMapping("/battery/bed_room")
    fun batteryMainRoom(@RequestParam unit: String, @RequestParam voltage: String): String {
        val batteryTest = BatteryBedRoom(unit, BigDecimal(voltage))
        batteryBedRoomDao.save(batteryTest)
        return "ok"
    }

    @Transactional
    @GetMapping("/battery/display")
    fun batteryDisplay(@RequestParam unit: String, @RequestParam voltage: String): String {
        val batteryTest = BatteryDisplay(unit, BigDecimal(voltage))
        batteryDisplayDao.save(batteryTest)
        return "ok"
    }

    @Transactional
    @GetMapping("/env/main_room")
    fun environmentMainRoom(@RequestParam temp: String, @RequestParam hum: String): String {
        val temperature = TemperatureMainRoom(BigDecimal(temp))
        temperatureMainRoomDao.save(temperature)
        val humidity = HumidityMainRoom(BigDecimal(hum))
        humidityMainRoomDao.save(humidity)
        return "ok"
    }

    @Transactional
    @GetMapping("/env/outside")
    fun environmentOutside(@RequestParam temp: String, @RequestParam hum: String): String {
        val temperature = TemperatureOutside(BigDecimal(temp))
        temperatureOutsideDao.save(temperature)
        val humidity = HumidityOutside(BigDecimal(hum))
        humidityOutsideDao.save(humidity)
        return "ok"
    }

    @GetMapping("/cron/show")
    fun showCrone(): String {
        return dynamicConfig?.lightCrone.toString() + ", " + dynamicConfig?.lightUrl.toString()
    }

    @GetMapping("/cron/{status}")
    fun croneOff(@PathVariable status: String): String {
        return when (status.toLowerCase()) {
            "off" -> {
                myAppConfig?.cancel()
                dynamicConfig?.lightCrone = "OFF"
                "OFF"
            }
            "on" -> {
                myAppConfig?.reschedule()
                "ON"
            }
            else -> "WRONG VALUE: $status USE: ON/OFF"
        }
    }

    @GetMapping("/cron")
    fun changeCrone(@RequestParam value: String, @RequestParam(required = false) url: String?): String {
        dynamicConfig?.lightCrone = value
        if (url != null) {
            dynamicConfig?.lightUrl = url
        }
        myAppConfig?.reschedule()
        return value
    }

    ///////////////////////////
    @GetMapping("/env/temp")
    fun temp(): List<Temperature> {
        return temperatureDao.findFirst100ByOrderByTimestampDesc()
    }

    @GetMapping("/env/hum")
    fun hum(): List<Humidity> {
        return humidityDao.findFirst100ByOrderByTimestampDesc()
    }

    @GetMapping("/env/temp/current")
    fun tempCurrent(): String {
        return average(temperatureDao.findFirst3ByOrderByTimestampDesc().map { temp -> temp.temp }.toList())
    }


    @GetMapping("/env/hum/current")
    fun humCurrent(): String {
        return average(humidityDao.findFirst3ByOrderByTimestampDesc().map { hum -> hum.hum }.toList())
    }

    //////////////////////////
    @GetMapping("/env/temp/main_room")
    fun tempMainRoom(): List<TemperatureMainRoom> {
        return temperatureMainRoomDao.findFirst100ByOrderByTimestampDesc()
    }

    @GetMapping("/env/hum/main_room")
    fun humMainRoom(): List<HumidityMainRoom> {
        return humidityMainRoomDao.findFirst100ByOrderByTimestampDesc()
    }

    @GetMapping("/env/temp/main_room/current")
    fun tempMainRoomCurrent(): String {
        return average(temperatureMainRoomDao.findFirst3ByOrderByTimestampDesc().map { temp -> temp.temp }.toList())
    }

    @GetMapping("/env/hum/main_room/current")
    fun humMainRoomCurrent(): String {
        return average(humidityMainRoomDao.findFirst3ByOrderByTimestampDesc().map { hum -> hum.hum }.toList())
    }

    //////////////////////////
    @GetMapping("/env/temp/outside")
    fun tempOutside(): List<TemperatureOutside> {
        return temperatureOutsideDao.findFirst100ByOrderByTimestampDesc()
    }

    @GetMapping("/env/hum/outside")
    fun humOutside(): List<HumidityOutside> {
        return humidityOutsideDao.findFirst100ByOrderByTimestampDesc()
    }

    @GetMapping("/env/temp/outside/current")
    fun tempOutsideCurrent(): String {
        return average(temperatureOutsideDao.findFirst3ByOrderByTimestampDesc().map { temp -> temp.temp }.toList())
    }

    @GetMapping("/env/hum/outside/current")
    fun humOutsideCurrent(): String {
        return average(humidityOutsideDao.findFirst3ByOrderByTimestampDesc().map { hum -> hum.hum }.toList())
    }

    private fun average(list: List<BigDecimal?>): String {
        return list.fold(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal(list.size), 2, RoundingMode.HALF_UP).toString()
    }

}