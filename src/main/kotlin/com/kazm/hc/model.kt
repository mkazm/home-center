package com.kazm.hc

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Humidity(
        var hum: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface HumidityDao : CrudRepository<Humidity, Long> {
    fun findFirst100ByOrderByTimestampDesc(): List<Humidity>
    fun findFirst3ByOrderByTimestampDesc(): List<Humidity>
}

@Entity
data class Temperature(
        var temp: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface TemperatureDao : CrudRepository<Temperature, Long> {
    fun findFirst100ByOrderByTimestampDesc(): List<Temperature>
    fun findFirst3ByOrderByTimestampDesc(): List<Temperature>
}

////MAIN ROOM/////

@Entity
@Table(name = "humidity_main_room")
data class HumidityMainRoom(
        var hum: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface HumidityMainRoomDao : CrudRepository<HumidityMainRoom, Long> {
    fun findFirst100ByOrderByTimestampDesc(): List<HumidityMainRoom>
    fun findFirst3ByOrderByTimestampDesc(): List<HumidityMainRoom>
}

@Entity
@Table(name = "temperature_main_room")
data class TemperatureMainRoom(
        var temp: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface TemperatureMainRoomDao : CrudRepository<TemperatureMainRoom, Long> {
    fun findFirst100ByOrderByTimestampDesc(): List<TemperatureMainRoom>
    fun findFirst3ByOrderByTimestampDesc(): List<TemperatureMainRoom>
}

////OUTSIDE/////

@Entity
@Table(name = "humidity_outside")
data class HumidityOutside(
        var hum: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface HumidityOutsideDao : CrudRepository<HumidityOutside, Long> {
    fun findFirst100ByOrderByTimestampDesc(): List<HumidityOutside>
    fun findFirst3ByOrderByTimestampDesc(): List<HumidityOutside>
}

@Entity
@Table(name = "temperature_outside")
data class TemperatureOutside(
        var temp: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface TemperatureOutsideDao : CrudRepository<TemperatureOutside, Long> {
    fun findFirst100ByOrderByTimestampDesc(): List<TemperatureOutside>
    fun findFirst3ByOrderByTimestampDesc(): List<TemperatureOutside>
}

///BATTERY////

@Entity
@Table(name = "battery")
data class Battery(
        var unit: String? = null,
        var voltage: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface BatteryDao : CrudRepository<Battery, Long> {
}

@Entity
@Table(name = "battery_outside")
data class BatteryOutside(
        var unit: String? = null,
        var voltage: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface BatteryOutsideDao : CrudRepository<BatteryOutside, Long> {
}

@Entity
@Table(name = "battery_bed_room")
data class BatteryBedRoom(
        var unit: String? = null,
        var voltage: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface BatteryBedRoomDao : CrudRepository<BatteryBedRoom, Long> {
}

@Entity
@Table(name = "battery_display")
data class BatteryDisplay(
        var unit: String? = null,
        var voltage: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface BatteryDisplayDao : CrudRepository<BatteryDisplay, Long> {
}

@Entity
@Table(name = "moisture")
data class Moisture(
        var plant: String? = null,
        var moisture: BigDecimal? = null,
        var timestamp: LocalDateTime? = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)

@Repository
interface MoistureDao : CrudRepository<Moisture, Long> {
}