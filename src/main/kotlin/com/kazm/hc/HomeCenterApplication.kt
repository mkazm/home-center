package com.kazm.hc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class HomeCenterApplication

fun main(args: Array<String>) {
    SpringApplication.run(HomeCenterApplication::class.java, *args)
}
