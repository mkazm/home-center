package com.kazm.hc

import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletResponse
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import javax.servlet.http.HttpServletRequest
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.scheduling.config.ScheduledTaskRegistrar
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.scheduling.config.ScheduledTask
import org.springframework.scheduling.config.TriggerTask
import org.springframework.scheduling.support.CronTrigger
import org.springframework.web.client.RestTemplate
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.util.*
import java.util.concurrent.*

@Configuration
class TaxiFareMVCConfig : WebMvcConfigurer {

    @Autowired
    private val requestInterceptor: RequestInterceptor? = null

    override fun addInterceptors(registry: InterceptorRegistry?) {
        registry!!.addInterceptor(requestInterceptor)
    }

    @Bean
    fun requestQueue(): ConcurrentLinkedQueue<Request> = ConcurrentLinkedQueue(ArrayList<Request>(200))

    @Bean
    fun restTemplate(builder: RestTemplateBuilder): RestTemplate {
        return builder.build()
    }

}

@Configuration
@EnableSwagger2
class SwaggerConfig {
    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
    }
}

@Component
class RequestInterceptor : HandlerInterceptorAdapter() {

    @Autowired
    private val requestQueue: ConcurrentLinkedQueue<Request>? = null

    private val log = logger()

    override fun preHandle(
            request: HttpServletRequest,
            response: HttpServletResponse,
            handler: Any): Boolean {

        var ipAddress = request.getHeader("X-FORWARDED-FOR")
        if (ipAddress == null) {
            ipAddress = request.remoteAddr
        }
        log.info("request: $ipAddress")
        if (requestQueue?.size!! >= 200) {
            requestQueue?.poll()
        }
        requestQueue?.add(Request(ipAddress, request.requestURI))
        return true
    }
}

@Component
class DynamicConfigs {

    var lightCrone: String? = null
    var lightUrl: String? = null

}

@Configuration
@EnableScheduling
class MyAppConfig : SchedulingConfigurer {

    @Autowired
    internal var env: Environment? = null

    @Value("\${light.url}")
    private val lightUtl: String? = null

    @Value("\${light.crone}")
    private val lightCrone: String? = null

    @Autowired
    private val restTemplate: RestTemplate? = null

    @Autowired
    private val dynamicConfigs: DynamicConfigs? = null

    private var taskRegistrar: ScheduledTaskRegistrar? = null

    @Bean(destroyMethod = "shutdown")
    fun taskExecutor(): Executor {
        return Executors.newScheduledThreadPool(10)
    }

    private var task: ScheduledTask? = null

    override fun configureTasks(taskRegistrar: ScheduledTaskRegistrar) {
        this.taskRegistrar = taskRegistrar
        this.taskRegistrar?.setScheduler(taskExecutor())
        val cron = if (dynamicConfigs?.lightCrone != null) dynamicConfigs?.lightCrone else lightCrone
        val trigger = CronTrigger(cron.toString())
        task = this.taskRegistrar?.scheduleTriggerTask(TriggerTask(RestAskTask(restTemplate, lightUtl), trigger))
    }

    fun reschedule() {
        task?.cancel()
        taskRegistrar?.setScheduler(taskExecutor())
        val cron = if (dynamicConfigs?.lightCrone != null) dynamicConfigs?.lightCrone else lightCrone
        val url = if (dynamicConfigs?.lightUrl != null) dynamicConfigs?.lightUrl else lightUtl
        val trigger = CronTrigger(cron.toString())
        task = taskRegistrar?.scheduleTriggerTask(TriggerTask(RestAskTask(restTemplate, url), trigger))
    }

    fun cancel() {
        task?.cancel()
    }
}

class RestAskTask(private val restTemplate: RestTemplate?, private val url: String?) : Runnable {
    override fun run() {
        val response = restTemplate?.getForEntity(url.toString(), String::class.java)
        println("elo $response")
    }
}



