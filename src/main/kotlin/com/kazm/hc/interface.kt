package com.kazm.hc

import com.vaadin.annotations.Theme
import com.vaadin.icons.VaadinIcons
import com.vaadin.server.Sizeable
import com.vaadin.server.VaadinRequest
import com.vaadin.spring.annotation.SpringUI
import com.vaadin.shared.ui.ValueChangeMode
import com.vaadin.ui.*
import com.vaadin.ui.Grid
import java.util.concurrent.ConcurrentLinkedQueue


@SpringUI(path = "/hc")
@Theme("valo")
class VaadinUI(val addNewBtn: Button = Button("New customer", VaadinIcons.PLUS),
               val grid: Grid<Request> = Grid(Request::class.java),
               val filter: TextField = TextField(),
               val requestQueue: ConcurrentLinkedQueue<Request>) : UI() {

    override fun init(request: VaadinRequest) {
        // build layout
        val actions = HorizontalLayout(filter, addNewBtn)
        val mainLayout = VerticalLayout(actions, grid)
        content = mainLayout

        grid.setHeight(300f, Sizeable.Unit.PIXELS)
        grid.setColumns("timestamp", "uri", "ip")

        filter.setPlaceholder("Filter by last name")

        // Hook logic to components

        // Replace listing with filtered content when user changes filter
        filter.setValueChangeMode(ValueChangeMode.LAZY)
        filter.addValueChangeListener { e -> listRequests(e.getValue()) }


        // Initialize listing
        listRequests("")
    }

    fun listRequests(request: String) {
        grid.setItems(requestQueue.toList())
    }
}